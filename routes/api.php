<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {

    $api->group(['namespace' => 'App\Http\Controllers'], function ($api) {
        // /auth
        $api->group(['prefix' => 'auth'], function ($api) {
            $api->post('/login', 'AuthController@login');
            $api->post('/signup', 'AuthController@signup');
            $api->post('/{id}/reset_passord', [ 'middleware' => 'auth:api', 'uses' => 'AuthController@resetPassword']);
        });

        $api->group([ 'middleware' => 'auth:api', 'prefix' => 'item'], function ($api) {
            $api->get('/items', 'TaskController@index');
            $api->post('/store', 'TaskController@store');
            $api->put('/{id}', 'TaskController@update');
            $api->get('/analytics', 'TaskController@analytics');
            $api->delete('/{id}', 'TaskController@destroy');

        });

        $api->group([ 'middleware' => 'auth:api', 'prefix' => 'randomizer'], function ($api) {
            $api->get('/', 'IntegerRandomizerController@get');
            $api->get('/sum', 'IntegerRandomizerController@sum');
            $api->get('/sort', 'IntegerRandomizerController@sort');
            $api->get('/average', 'IntegerRandomizerController@average');
            $api->post('/randomize', 'IntegerRandomizerController@randomize');
        });
    });
});

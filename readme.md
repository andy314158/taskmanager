## Task Manager API and VueJS Client
Contains mysql db, task manager api and the vuejs frontend client for managing tasks

### Provisioning a local environment
Requires docker.

Just run: `docker-compose up`

Then..
run: cd client
run: npm install
run: npm run dev

### Deployment
See `bitbucket-pipelines.yml` and `Dockerfile.deploy`


### Notes
If you run into problems susch as "autoload php errors" simply enter the terminal for task_manager_app with in the docker client and rerun 
composer install

Bitbucket pipelines have been built to launch application to aws, however in the current repo it does not do so as i already have a personal application running on aws with the free tier plan, this repo also contains ther service update script to update the ecs with the newly pushed ecr image from bitbucket.

In app tasks do not refresh on emit when added, but for some reason do when deleted or marked as completed, so you may have to refresh or switch tabs to 
see newly added tasks

The second test of the coding challenge is included in the api as a repo, but not completly done as I havnt found the time to complete that today.



### Important third parties
- dingo/api - Makes managing APIs easier
- tymon/jwt-auth - Auth tokens



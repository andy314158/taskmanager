<?php

namespace App\Http\Requests\API\Traits;

use Dingo\Api\Exception\ResourceException;
use Illuminate\Contracts\Validation\Validator;

trait APIRequest
{
    /**
     * {@inheritdoc}
     */
    protected function failedValidation(Validator $validator)
    {
        if ($this->is(config('api.prefix') . '/*') && (($this->ajax() && ! $this->pjax()) || $this->wantsJson())) {
            throw new ResourceException('Failed', $validator->getMessageBag());
        }

        parent::failedValidation($validator);
    }
}
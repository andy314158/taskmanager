<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\SignUpRequest;
use App\Repositories\AuthRepository;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class AuthController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $authRepository;

    /**
     * ActivityProviderController constructor.
     * @param null $activityProviderRepository
     */
    public function __construct($authRepository = null)
    {
        if ($authRepository === null) {
            $authRepository = new AuthRepository();
        }

        $this->authRepository = $authRepository;
    }

    public function login(LoginRequest $request)
    {
        return $this->authRepository->login();
    }

    public function signup(SignUpRequest $request)
    {
        $userInfo = $request->all();

        unset($userInfo['q']);

        return $this->authRepository->signup($userInfo);
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        return $this->authRepository->resetPassword();
    }
}

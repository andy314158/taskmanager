<?php

namespace App\Http\Controllers;

use App\Http\Requests\SortRequest;
use App\Repositories\IntegerRandomizerRepository;

class IntegerRandomizerController extends Controller
{

    protected $integerRandomizerRepository;

    /**
     * IntegerRandomizerController constructor.
     * @param null $integerRandomizerRepository
     */
    public function __construct($integerRandomizerRepository = null)
    {
        if ($integerRandomizerRepository === null) {
            $integerRandomizerRepository = new IntegerRandomizerRepository();
        }

        $this->integerRandomizerRepository = $integerRandomizerRepository;
    }

    public function get()
    {
        return response()->json($this->integerRandomizerRepository->get());
    }

    public function sum()
    {
        return response()->json($this->integerRandomizerRepository->sum());
    }

    public function sort(SortRequest $request)
    {
        return response()->json($this->integerRandomizerRepository->sort($request->get('sort')));

    }

    public function average()
    {
        return response()->json($this->integerRandomizerRepository->average());
    }

    public function randommizeArray()
    {
        return response()->json($this->integerRandomizerRepository->randommizeArray());
    }
}

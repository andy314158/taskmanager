<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Repositories\TaskRepository;

class TaskController extends Controller
{

    protected $taskRepository;

    /**
     * TaskController constructor.
     * @param null $taskRepository
     */
    public function __construct($taskRepository = null)
    {
        if ($taskRepository === null) {
            $taskRepository = new TaskRepository();
        }

        $this->taskRepository = $taskRepository;
    }

    /**
     * Display a listing tasks.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->taskRepository->index(auth()->user()->id);
    }

    /**
     * Store a newly created task.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTaskRequest $request)
    {
        return $this->taskRepository->store($request->get('name'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTaskRequest $request, $id)
    {
        return $this->taskRepository->update($id, $request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->taskRepository->destroy($id);
    }

     /**
     * Returns the burndownobject to be directly displayed by the vuechart.
     *
     * @return \Illuminate\Http\Response
     */
    public function analytics()
    {
        return response()->json($this->taskRepository->analytics(auth()->user()->id));
    }
}

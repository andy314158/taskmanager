<?php

namespace App\Repositories;

use App\Models\Task;
use Carbon\Carbon;
use stdClass;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TaskRepository
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($userId)
    {
        return Task::where('user_id', $userId)->orderBy('created_at', 'DESC')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($name)
    {
        $newTask = new Task;
        $newTask->name = $name;
        $newTask->user_id = auth()->user()->id;
        $newTask->save();

        return $newTask;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Models\Task
     */
    public function update($id, $taskData)
    {
        $existingTask = Task::find($id);

        if ($existingTask) {
            $existingTask->completed = isset($taskData['completed']) ? $taskData['completed'] : false;
            $existingTask->completed_at = isset($taskData['completed']) ? Carbon::now()->format('Y-m-d H:i:s') : null;
            $existingTask->save();
            return $existingTask;
        } else {
            return new NotFoundHttpException();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $existingTask = Task::find($id);

        if ($existingTask) {
            $existingTask->delete();
            return "Task successfully deleted.";
        }
        return new NotFoundHttpException();
    }

    /**
     * Returns the burndownobject for the vue chart to directly display (CMS).
     *
     * @param  int  $id
     */
    public function analytics($userId)
    {
        $burnDownObject = new stdClass();
        $burnDownObject->labels = [];
        $burnDownObject->datasets = [];
        $completedTasksPerMinute = [];
        $uncompletedTasksPerMinute = [];
        $currentlyUncompleted = Task::where('user_id', $userId)->where('completed', false)->count();

        for ($minutes = 0; $minutes <= 60; $minutes++) {

            $dateToFilterBy = Carbon::now();
            $dateToFilterBy->subMinutes($minutes)->format('Y-m-d H:i:s');
            array_push($burnDownObject->labels, sprintf('At %s', ($dateToFilterBy)));

            $completed = Task::where('user_id', $userId)->where('completed_at', '<', $dateToFilterBy)->count();
            $uncompleted = Task::where('user_id', $userId)->where('completed_at', '>', $dateToFilterBy)->count();


            array_push($completedTasksPerMinute, $completed);
            array_push($uncompletedTasksPerMinute, $uncompleted + $currentlyUncompleted);

        }

        $completesDatasetObject = new stdClass();
        $completesDatasetObject->label = "Completed Tasks";
        $completesDatasetObject->backgroundColor = "#e6e6e6";
        $completesDatasetObject->data = $completedTasksPerMinute;

        $uncompletesDatasetObject = new stdClass();
        $uncompletesDatasetObject->label = "uncompleted Tasks";
        $uncompletesDatasetObject->backgroundColor = "#e2e2e2";
        $uncompletesDatasetObject->data = $uncompletedTasksPerMinute;

        array_push($burnDownObject->datasets, $completesDatasetObject);
        array_push($burnDownObject->datasets, $uncompletesDatasetObject);


        return $burnDownObject;
    }

}

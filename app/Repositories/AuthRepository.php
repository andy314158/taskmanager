<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthRepository
{
    protected $locationRepository;

    protected $profileRepository;

    /**
     * ActivityProviderController constructor.
     * @param null $activityProviderRepository
     */
    public function __construct()
    {

    }

    public function login()
    {
        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['message' => 'Unauthorized'], 422);
        }

        return $this->respondWithToken($token);
    }


    public function signup($userData)
    {
        $user = User::where('email', $userData['email'])->first();

        if($user != null){
            return response()->json(['error' => 'User already has an account'], 409);
        }

        $userData['password'] = Hash::make($userData['password']);

        $user = User::create($userData);

        return $this->login();
    }

    public function resetPassword($credentials, $newPassword)
    {

    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }
}

<?php

namespace App\Repositories;

class IntegerRandomizerRepository
{

    private $arrayOfRandomIntegers;

    public function __construct()
    {
        $this->arrayOfRandomIntegers = [4, 20, 16, 82, 19, 34, 29, 27, 92, 100];
    }

    public function get()
    {
        return $this->arrayOfRandomIntegers;
    }

    public function sum()
    {
        $indexOne = rand(0, 9);
        $indexTwo = rand(0, 9);

        return $this->arrayOfRandomIntegers[$indexOne] + $this->arrayOfRandomIntegers[$indexTwo];
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sort($sort)
    {
        if('asc' == strtolower($sort)){
            sort($this->arrayOfRandomIntegers);
            return $this->arrayOfRandomIntegers;
        }
        else if('desc' == strtolower($sort)){
            rsort($this->arrayOfRandomIntegers);
            return $this->arrayOfRandomIntegers;
        }
        else{
            return $this->arrayOfRandomIntegers;
        }
    }

    public function average()
    {
        return array_sum($this->arrayOfRandomIntegers)/count($this->arrayOfRandomIntegers);
    }

    public function randommizeArray()
    {
        $this->arrayOfRandomIntegers = [];

        for($index = 0; $index < 10; $index++){
            array_push($this->arrayOfRandomIntegers, rand(0, 100));
        }

        return $this->arrayOfRandomIntegers;
    }

}

#!/bin/bash
	SERVICE_NAME="IceBreaker-ecs"
	IMAGE_VERSION="%BRANCH_NAME%_%BUILD_NUMBER%"
	TASK_FAMILY="incbreaker-taskdefn"
	CLUSTER="icebreaker"
	REGION="ap-southeast-2"

#Create a new task definition for this build
	echo 'Creating a new task definition for this build'
	sed -e "s;%BUILD_NUMBER%;${BITBUCKET_BUILD_NUMBER};g" -e "s;%BRANCH_NAME%;${BITBUCKET_BRANCH};g" icebreaker-taskdefn.json > icebreaker-td_${BITBUCKET_BUILD_NUMBER}.json
	#sed -e "s;%BRANCH_NAME%;${BRANCH_NAME};g" backoffice-frontend.json > backoffice-frontend-td_${BUILD_NUMBER}.json
	aws ecs register-task-definition --family ${TASK_FAMILY} --cli-input-json file://icebreaker-td_${BITBUCKET_BUILD_NUMBER}.json --region ${REGION}

# Update the service with the new task definition and desired count
	echo 'Updating the service with the new task definition and desired count...'
	TASK_REVISION=$(aws ecs describe-task-definition --task-definition ${TASK_FAMILY} --region ${REGION}  | jq --raw-output ".taskDefinition.revision")
	DESIRED_COUNT=$(aws ecs describe-services --services ${SERVICE_NAME} --region ${REGION} --cluster ${CLUSTER} |  jq --raw-output ".services[0].desiredCount")
	if [ "${DESIRED_COUNT}" = "0" ]; then
		DESIRED_COUNT="1"
	fi
	aws ecs update-service --cluster ${CLUSTER} --service ${SERVICE_NAME} --task-definition ${TASK_FAMILY}:${TASK_REVISION} --desired-count ${DESIRED_COUNT} --region ${REGION}

import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Login from '@/components/Login'
import Register from '@/components/Register'
import BurnDownChart from '@/components/BurnDownChart'
import IntegerRandomizer from '@/components/IntegerRandomizer'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/burndownchart',
      name: 'burndownchart',
      component: BurnDownChart
    },
    {
      path: '/IntegerRandomizer',
      name: 'IntegerRandomizer',
      component: IntegerRandomizer
    }
  ]
})

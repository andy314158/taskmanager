FROM php:7.4.0-fpm-alpine

ENV PACKAGES="nginx python3 py-pip libmcrypt-dev zlib-dev git"

RUN apk update

ENV PYTHONUNBUFFERED=1

RUN echo "**** install Python ****" && \
    apk add --no-cache python3 && \
    if [ ! -e /usr/bin/python ]; then ln -sf python3 /usr/bin/python ; fi && \
    \
    echo "**** install pip ****" && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --no-cache --upgrade pip setuptools wheel && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi

RUN apk update \
    && apk --no-cache --progress add $PACKAGES \
    && mkdir -p /run/nginx/ \
    && chmod ugo+w /run/nginx/ \
    && rm -f /etc/nginx/nginx.conf \
    && mkdir -p /etc/nginx/conf.d \
    && chmod -R 775 /var/www/ \
    && chown -R nginx:nginx /var/www/ \
    && pip install --upgrade pip \
    && pip install supervisor \
    && pip install supervisor-stdout \
    && rm /usr/local/etc/php-fpm.d/zz-docker.conf \
    && docker-php-ext-install pdo_mysql

RUN php -r "copy('https://getcomposer.org/download/2.0.11/composer.phar', 'composer.phar');"
RUN mv composer.phar /usr/bin/composer
RUN chmod +x /usr/bin/composer

COPY ./deploy/manifest/ /

COPY . /var/www/app
COPY .env.example /var/www/app/.env

WORKDIR /var/www/app

EXPOSE 80

RUN mkdir -p /var/www/app/bootstrap/cache \
    && chown -R www-data:www-data /var/www/app/bootstrap/cache \
    && chmod -R 755 /var/www/app/bootstrap/cache

RUN composer update

RUN chown -R www-data:www-data /var/www/app/storage \
    && chmod -R 755 /var/www/app/storage \
    && mkdir /var/lib/nginx/tmp/client_body \
    && chmod -R 777 /var/lib/nginx/tmp \
    && chown -R www-data:www-data /var/lib/nginx \
    && php artisan view:clear \
    && php artisan config:clear \
    && php artisan route:clear \
    && php artisan config:cache \
    && php artisan key:generate \
    && php artisan config:cache \
    && php artisan jwt:secret \
    && php artisan api:cache

CMD ["/bin/sh", "/entrypoint.sh"]
